package org.comsoft.gztls;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.impl.SimpleLog;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.AbstractHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.BasicClientConnectionManager;
import org.apache.http.util.EntityUtils;

import javax.net.ssl.SSLContext;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

/**
 * Created by alexgri on 27.07.15.
 */
public class GZClient {

	public static final String URI = "https://int223.zakupki.gov.ru/223/integration/integration/upload";
	Log log = new SimpleLog("GZClient");
	public void getContentUsingTlsConnection() {
		//CProTLSParams tlsParams = new CProTLSParams("CertStore", trustCerts, pwd, "HDImageStore", "le-aaca7af5-b3f0-4b90-882b-b558cbe8ccba", "12345678", null);

	}

	private SSLContext sslContextFromParams(CProTLSParams tlsParams) throws CertificateException, UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException, IOException {
		CProAdvManager manager = new CProAdvManager();
		SSLContext sslContext = manager.cProAwaredSSLContext(tlsParams);
		return sslContext;
	}

	private AbstractHttpClient httpClient(SSLContext sslContext) {
		org.apache.http.conn.ssl.SSLSocketFactory sslSocketFactory = new org.apache.http.conn.ssl.SSLSocketFactory(sslContext);
		SchemeRegistry registry = new SchemeRegistry();
		registry.register(new Scheme("http", 80, PlainSocketFactory.getSocketFactory()));
		registry.register(new Scheme("https", 443, sslSocketFactory));
		//registry.register(new Scheme("https", 4444, sslSocketFactory));

		ClientConnectionManager clientConnectionManager = new BasicClientConnectionManager(registry);
		DefaultHttpClient httpclient = new DefaultHttpClient(clientConnectionManager);
		return httpclient;
	}

	public String executeRequest(HttpClient httpclient, HttpUriRequest uriRequest) {

		if (log.isDebugEnabled()) {
			log.debug("----------------------------------------");
			Header[] headers = uriRequest.getAllHeaders();
			for (int i = 0; i < headers.length; i++) {
				log.debug(headers[i]);
			}
			log.debug("----------------------------------------");
		}
		try {
			// specify the host, protocol, and port
			//HttpResponse httpResponse = httpclient.execute(target, get);
			HttpResponse httpResponse = httpclient.execute(uriRequest);
			HttpEntity entity = httpResponse.getEntity();

			if (log.isDebugEnabled()) {
				log.debug("----------------------------------------");
				log.debug(httpResponse.getStatusLine());
				Header[] headers = httpResponse.getAllHeaders();
				for (int i = 0; i < headers.length; i++) {
					log.debug(headers[i]);
				}
				log.debug("----------------------------------------");
			}

			if (entity != null) {
				String result = EntityUtils.toString(entity, "UTF8");
				//String result = EntityUtils.toString(entity, "windows-1251");
				log.debug(result);
				return result;
			}
			return "";
		} catch (IOException e) {
			log.error(e);
			return "";
		} finally {
			// When HttpClient instance is no longer needed,
			// shut down the connection manager to ensure
			// immediate deallocation of all system resources
			httpclient.getConnectionManager().shutdown();
		}
	}

	public HttpPost preparePost(String requsetFilePath, GZParams gzParams) throws IOException, URISyntaxException {

		File originFile = new File(requsetFilePath);

		log.debug("preparing xml for sending\n " + originFile.toString());

		FileBody fileBody = new FileBody(originFile, originFile.getName(), "text/xml", "UTF-8");
		MultipartEntity multipartEntity = new MultipartEntity();
		multipartEntity.addPart("document", fileBody);
		multipartEntity.addPart("login", new StringBody(gzParams.getLogin()));
		multipartEntity.addPart("password", new StringBody(gzParams.getPassword()));

		HttpPost post = new HttpPost(URI);
		post.setEntity(multipartEntity);
		return post;
	}

	public String executeRequest(CProTLSParams cProTLSParams, ProxyParams proxyParams, GZParams gzParams, String requsetFilePath) throws CertificateException, UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException, IOException, URISyntaxException {
		SSLContext sslContext = sslContextFromParams(cProTLSParams);
		AbstractHttpClient httpClient = httpClient(sslContext);
		if (proxyParams != null) {
			httpClient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxyParams.getProxyHost());
			if (proxyParams.getCredentialsProvider() != null)
				httpClient.setCredentialsProvider(proxyParams.getCredentialsProvider());
		}
		String response = executeRequest(httpClient, preparePost(requsetFilePath, gzParams));
		log.info(response);
		return response;
	}



}
