package org.comsoft.gztls;

/**
 * Created by alexgri on 07.07.15.
 */
public class CProTLSParams {
	private final String trustStoreType;
	private final String trustStore;
	private final String trustStorePassword;

	private final String keyStoreType;
	private final String keyStoreAlias;
	private final String keyStorePassword;
	private final String keyStore;

	public CProTLSParams(String trustStoreType, String trustStore, String trustStorePassword, String keyStoreType, String keyStoreAlias, String keyStorePassword, String keyStore) {
		this.trustStoreType = trustStoreType;
		this.trustStore = trustStore;
		this.trustStorePassword = trustStorePassword;
		this.keyStoreType = keyStoreType;
		this.keyStoreAlias = keyStoreAlias;
		this.keyStorePassword = keyStorePassword;
		this.keyStore = keyStore;
	}

	public CProTLSParams(String trustStore, String trustStorePassword, String keyStoreAlias, String keyStorePassword) {
		this.trustStoreType = "CertStore";
		this.keyStoreType = "HDImageStore";
		this.keyStore = null;

		this.trustStore = trustStore;
		this.trustStorePassword = trustStorePassword;
		this.keyStoreAlias = keyStoreAlias;
		this.keyStorePassword = keyStorePassword;
	}

	public CProTLSParams(String trustStore, String trustStorePassword, String keyStoreAlias, String keyStorePassword, String keyStore) {
		this.trustStoreType = "CertStore";
		this.keyStoreType = "HDImageStore";

		this.trustStore = trustStore;
		this.trustStorePassword = trustStorePassword;
		this.keyStoreAlias = keyStoreAlias;
		this.keyStorePassword = keyStorePassword;
		this.keyStore = keyStore;
	}

	public String getTrustStoreType() {
		return trustStoreType;
	}

	public String getTrustStore() {
		return trustStore;
	}

	public String getTrustStorePassword() {
		return trustStorePassword;
	}

	public String getKeyStoreType() {
		return keyStoreType;
	}

	public String getKeyStoreAlias() {
		return keyStoreAlias;
	}

	public String getKeyStorePassword() {
		return keyStorePassword;
	}

	public String getKeyStore() {
		return keyStore;
	}

	public char[] getKeyStorePasswordChars() {
		return getKeyStorePassword().toCharArray();
	}

	public char[] getTrustStorePasswordChars() {
		return trustStorePassword == null ? null : trustStorePassword.toCharArray();
	}
}
