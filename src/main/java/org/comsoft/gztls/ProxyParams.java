package org.comsoft.gztls;

import org.apache.http.HttpHost;
import org.apache.http.client.CredentialsProvider;

/**
 * Created by alexgri on 28.07.15.
 */
public class ProxyParams {
	private final HttpHost proxyHost;
	private final CredentialsProvider credentialsProvider;

	public ProxyParams(HttpHost proxyHost, CredentialsProvider credentialsProvider) {
		this.proxyHost = proxyHost;
		this.credentialsProvider = credentialsProvider;
	}

	public HttpHost getProxyHost() {
		return proxyHost;
	}

	public CredentialsProvider getCredentialsProvider() {
		return credentialsProvider;
	}
}
