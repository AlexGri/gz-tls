package org.comsoft.gztls;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.apache.commons.io.Charsets;
import org.apache.commons.io.FileUtils;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.prefs.Preferences;

/**
 * Created by alexgri on 28.07.15.
 */
public class Main {

	public static final String LOGLEVEL = "org.apache.commons.logging.simplelog.defaultlog";
	public static final String SHOW_LOG_NAME = "org.apache.commons.logging.simplelog.showlogname";

	public static final String GZUSER = "gzuser";
	public static final String GZPASSWORD = "gzpassword";

	public static void main(String[] args) throws CertificateException, UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException, IOException, URISyntaxException {
		if (args.length == 0)
			throw new IllegalArgumentException("usage: java -jar gz-tls-version-jar-with-dependencies.jar pathToCustomConfigFile.conf");

		String customConfigPath = args[0];
		Config customConfig = ConfigFactory.parseFile(new File(customConfigPath));

		String requsetFilePath = args[1];
		String responseFilePath = args[2];
		File responseFile = new File(responseFilePath);

		Config config = customConfig.withFallback(ConfigFactory.load());

		System.setProperty(LOGLEVEL, config.getString(LOGLEVEL));
		System.setProperty(SHOW_LOG_NAME, config.getString(SHOW_LOG_NAME));

		configureCerificateRevocation(config);

		String trustStoreType = config.getString("trustStoreType");
		String trustStore = nullable(config, "trustStore");
		String trustStorePwd = nullable(config, "trustStorePwd");
		String keyStoreType = config.getString("keyStoreType");
		String keyStoreAlias = nullable(config, "keyStoreAlias");
		String keyStorePassword = nullable(config, "keyStorePassword");
		String keyStore = nullable(config, "keyStore");

		GZParams gzParams = gzParams(config);

		ProxyParams proxyParams = proxyParams(config);

		CProTLSParams tlsParams = new CProTLSParams(trustStoreType, trustStore, trustStorePwd, keyStoreType, keyStoreAlias, keyStorePassword, keyStore);
		GZClient gzClient = new GZClient();
		String response = gzClient.executeRequest(tlsParams, proxyParams, gzParams, requsetFilePath);
		FileUtils.writeStringToFile(responseFile, response, Charsets.UTF_8);
	}

	private static void configureCerificateRevocation(Config config) {
		Preferences node = Preferences.userRoot().node("ru/CryptoPro/ssl");
		String revocationDefault = "Enable_revocation_default";
		String revocationValue = nullable(config, revocationDefault);
		if (revocationValue != null)
			node.put(revocationDefault, revocationValue);
	}

	private static String nullable(Config config, String paramName) {
		return config.hasPath(paramName) ? config.getString(paramName) : null;
	}

	private static ProxyParams proxyParams(Config config) {
		String host = nullable(config, "https.proxyHost");
		Integer port = config.hasPath("https.proxyPort") ? config.getInt("https.proxyPort") : null;
		if (host != null && port != null) {
			HttpHost proxyHost = new HttpHost(host, port);
			CredentialsProvider credentialsProvider = null;
			String proxyUser = nullable(config, "https.proxyUser");
			String proxyPassword = nullable(config, "https.proxyPassword");
			if (proxyUser != null) {
				Credentials credentials = new UsernamePasswordCredentials(proxyUser, proxyPassword);
				credentialsProvider = new BasicCredentialsProvider();
				credentialsProvider.setCredentials(new AuthScope(proxyHost), credentials);
			}
			return new ProxyParams(proxyHost, credentialsProvider);
		}
		return null;
	}



	private static GZParams gzParams(Config config) {
		String gzuser = config.getString(GZUSER);
		String gzpassword = config.getString(GZPASSWORD);
		return new GZParams(gzuser, gzpassword);
	}
}
