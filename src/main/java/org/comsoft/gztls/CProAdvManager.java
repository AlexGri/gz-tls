package org.comsoft.gztls;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.impl.SimpleLog;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.*;
import java.security.cert.CertificateException;
import java.util.Enumeration;

/**
 * Created by alexgri on 07.07.15.
 */
public class CProAdvManager {
	Log log = new SimpleLog("CProAdvManager");

	public SSLContext cProAwaredSSLContext(CProTLSParams tlsParams) throws KeyStoreException, CertificateException, NoSuchAlgorithmException, IOException, KeyManagementException, UnrecoverableKeyException {
		//System.setProperty("javax.net.ssl.keyStoreType","HDImageStore");
		//System.setProperty("javax.net.ssl.keyStorePassword","password");
		//String pwd = "password";

		KeyStore keyStore = KeyStore.getInstance(tlsParams.getKeyStoreType());
		InputStream resourceAsStream = null;
		try {
			if (tlsParams.getKeyStore() != null) {
				resourceAsStream = new FileInputStream(tlsParams.getKeyStore());
			}
			//keyStore.load(resourceAsStream, pwd.toCharArray());
			keyStore.load(resourceAsStream, tlsParams.getKeyStorePasswordChars());
		} finally {
			IOUtils.closeQuietly(resourceAsStream);
		}



		KeyStore trustStore = KeyStore.getInstance(tlsParams.getTrustStoreType());
		InputStream in = null;
		try {
			if (tlsParams.getTrustStore() != null)
				in = new FileInputStream(tlsParams.getTrustStore());
			trustStore.load(in, tlsParams.getTrustStorePasswordChars());
		} finally {
			IOUtils.closeQuietly(in);
		}

		if (log.isDebugEnabled()) {
			Enumeration<String> ksAliases = keyStore.aliases();
			while (ksAliases.hasMoreElements()) {
				String als = ksAliases.nextElement();
				log.debug("ksAliases = " + als);
			}

			Enumeration<String> aliases = trustStore.aliases();
			while (aliases.hasMoreElements()) {
				log.debug("aliases = " + aliases.nextElement());
			}
		}


		KeyManagerFactory kmf = KeyManagerFactory.getInstance("GostX509");
		kmf.init(keyStore, tlsParams.getKeyStorePasswordChars());

		log.info("kmf inited");

		TrustManagerFactory tmf = TrustManagerFactory.getInstance("GostX509");
		tmf.init(trustStore);

		log.info("tmf inited");


		SSLContext sslContext = SSLContext.getInstance("GostTLS");
		SecureRandom secureRandom = new SecureRandom();
		sslContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), secureRandom);
		return sslContext;
	}
}
