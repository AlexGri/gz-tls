package org.comsoft.gztls;

/**
 * Created by alexgri on 05.08.15.
 */
public class GZParams {
	public final String login;
	public final String password;

	public GZParams(String login, String password) {
		this.login = login;
		this.password = password;
	}

	public String getLogin() {
		return login;
	}

	public String getPassword() {
		return password;
	}
}
