package org.comsoft.gztls;

import java.util.prefs.Preferences;

/**
 * Created by alexgri on 07.07.15.
 */
public class CproBasicManager {
	//System.setProperty("com.sun.security.enableCRLDP", "true");
	//System.setProperty("com.ibm.security.enableCRLDP", "true");

	public static void applyTLSParams(CProTLSParams params) {
		System.setProperty("javax.net.ssl.trustStoreType", params.getTrustStoreType());
		System.setProperty("javax.net.ssl.trustStore", params.getTrustStore()); // Содержит корневой сертификат https://tlsgost-2001auth.cryptopro.ru/index.html (Тестовый УЦ)
		//System.setProperty("javax.net.ssl.trustStore", fullPath("/mtr-resources/rootcastore")); // Содержит корневой сертификат https://tlsgost-2001auth.cryptopro.ru/index.html (Тестовый УЦ)
		System.setProperty("javax.net.ssl.trustStorePassword", params.getTrustStorePassword());

		// При двухстороннней аутентификации (сертификат клиента выпущен в Тестовом УЦ)
		System.setProperty("javax.net.ssl.keyStoreType", params.getKeyStoreType());
		System.setProperty("javax.net.ssl.keyStoreAlias", params.getKeyStoreAlias());
		System.setProperty("javax.net.ssl.keyStorePassword", params.getKeyStorePassword());
	}

	public static void disableCertificateRevocationCheck() {
		Preferences p = Preferences.userRoot().node("ru/CryptoPro/ssl");
		p.put("Enable_revocation_default", "false");
	}
}
